;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;; Place your private configuration here! Remember, you do not need to run 'doom
;; sync' after modifying this file!


;; Some functionality uses this to identify you, e.g. GPG configuration, email
;; clients, file templates and snippets.
(setq user-full-name "Junpei Tajima"
      user-mail-address "p-baleine@gmail.com")

;; Doom exposes five (optional) variables for controlling fonts in Doom. Here
;; are the three important ones:
;;
;; + `doom-font'
;; + `doom-variable-pitch-font'
;; + `doom-big-font' -- used for `doom-big-font-mode'; use this for
;;   presentations or streaming.
;;
;; They all accept either a font-spec, font string ("Input Mono-12"), or xlfd
;; font string. You generally only need these two:
(setq doom-font (font-spec :family "Noto Mono" :size 14))
(setq doom-unicode-font (font-spec :family "Noto Sans Mono CJK JP" :size 14))

;; There are two ways to load a theme. Both assume the theme is installed and
;; available. You can either set `doom-theme' or manually load a theme with the
;; `load-theme' function. This is the default:
(setq doom-theme 'doom-nord)

;; If you use `org' and don't want your org files in the default location below,
;; change `org-directory'. It must be set before org loads!
(setq org-directory (expand-file-name  "~/Dropbox/org"))

;; This determines the style of line numbers in effect. If set to `nil', line
;; numbers are disabled. For relative line numbers, set this to `relative'.
(setq display-line-numbers-type nil)


;; Here are some additional functions/macros that could help you configure Doom:
;;
;; - `load!' for loading external *.el files relative to this one
;; - `use-package' for configuring packages
;; - `after!' for running code after a package has loaded
;; - `add-load-path!' for adding directories to the `load-path', relative to
;;   this file. Emacs searches the `load-path' when you load packages with
;;   `require' or `use-package'.
;; - `map!' for binding new keys
;;
;; To get information about any of these functions/macros, move the cursor over
;; the highlighted symbol at press 'K' (non-evil users must press 'C-c g k').
;; This will open documentation for it, including demos of how they are used.
;;
;; You can also try 'gd' (or 'C-c g d') to jump to their definition and see how
;; they are implemented.

;; 設定ファイルが太ってきたら、個別の private module に分けていく

;; tab-bar は表示しない
(setq tab-bar-show nil)


;; other-window の代替
(use-package! ace-window
  :defer t
  :commands (ace-window)
  :bind
  (("C-M-o" . ace-window))
  :custom
  (aw-keys '(?j ?k ?l ?i ?o ?h ?y ?u ?p))
  :custom-face
  (aw-leading-char-face ((t (:height 4.0 :foreground "#f1fa8c")))))


;; 日記
(use-package org-journal
  :defer t
  :custom
  (org-journal-dir (expand-file-name "~/Dropbox/journal/"))
  (org-journal-file-format "%Y%m%d.org")
  (org-journal-cache-file (concat doom-cache-dir "org-journal")))


;; treemacs
(use-package treemacs
  :defer t
  :custom
  ;; treemacs も等幅フォントで表示する
  (doom-themes-treemacs-enable-variable-pitch nil)
  :config
  (progn
    (treemacs-follow-mode +1)

    ;; treemacs でカーソルが全く見えないことへの対応
    ;;
    ;; https://github.com/hlissner/doom-emacs/commit/115d1990d49a6cc1df3158be1a89fb2af626b433#diff-5a7c5f78f54a18743b2eb5b17941b9d3
    ;;
    ;; 上記コミットメッセージにある通り
    ;; "This causes more issues that solves."
    ;; らしいので注意
    (defun +treemacs|improve-hl-line-contrast ()
      "`hl-line' doesn't stand out enough in some themes."
      (face-remap-add-relative 'hl-line 'region))
    (add-hook 'treemacs-mode-hook #'+treemacs|improve-hl-line-contrast)))


;; lang

;; JavaScript
(use-package! js2-mode
  :custom
  (js-indent-level 2))


;; ui

;; modeline
(use-package! doom-modeline
  :custom
  (doom-modeline-major-mode-icon t)
  (doom-modeline-major-mode-color-icon nil)
  (doom-modeline-buffer-file-name-style 'truncate-with-project)
  :config
  (let ((custom-bg (doom-darken (doom-color 'dark-blue) 0.4)))
    (custom-set-faces!
      `(mode-line :background ,custom-bg)
      `(solaire-mode-line-face :background ,custom-bg)
      `(doom-modeline-buffer-major-mode
        :foreground ,(doom-color 'orange) :bold t)
      `(doom-modeline-bar :background ,(doom-color 'orange))))
  :after solaire-mode)


;; company
(use-package! company
  :defer t
  :config
  (progn
    (custom-set-faces
     `(company-tooltip
       ((t (:inherit default
            :background ,(doom-darken (doom-color 'dark-blue) 0.5)))))
     `(company-tooltip-selection
       ((t (:inherit font-lock-function-name-face))))
     `(company-tooltip-common
       ((t (:inherit font-lock-constant-face))))
     `(company-tooltip-annotation
       ((t (:foreground ,(doom-lighten (doom-color 'yellow) 0.4))))))))


;; lsp-ui
(use-package! lsp-ui
  :config
  ;; flycheck の代わりに flymake を使う
  (setq lsp-prefer-flymake t)
  :custom
  (lsp-ui-flycheck-enable nil))


;; flymake-posframe
;; flymake のメッセージを inline で表示する
(use-package! flymake-posframe
  :config
  (progn
    (use-package! posframe)

    (custom-set-faces
     `(flymake-posframe-background-face
       ((t (:inherit default
            :background ,(doom-darken (doom-color 'dark-blue) 0.5)))))
     `(flymake-posframe-foreground-face
       ((t (:foreground ,(doom-lighten (doom-color 'yellow) 0.4)))))))
  :hook (flymake-mode . flymake-posframe-mode))


(load! "+emacs")
