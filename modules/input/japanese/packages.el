;;; $DOOMDIR/modules/input/japanese/config.el -*- lexical-binding: t; -*-

(package! ddskk)
(package! migemo)
(package! avy-migemo :pin "922a6dd82c")
(package! pangu-spacing :pin "f92898949b")
