;;; input/japanese/config.el -*- lexical-binding: t; -*-

;; migemo 、ローマ字で日本語検索する
(use-package! migemo
  :config
  (progn
    (when (executable-find migemo-command)
      (migemo-init)))
  :custom
  (migemo-dictionary "/usr/local/share/migemo/utf-8/migemo-dict")
  (migemo-user-dictionary nil)
  (migemo-regex-dictionary nil)
  (migemo-directory (concat doom-etc-dir "migemo/"))
  (migemo-coding-system 'utf-8-unix))


;; ivy 系、検索と補完
(use-package! ivy)
(use-package! avy)
(use-package! counsel)
(use-package! avy-migemo
  ;; This package should not marked as `defer' to use migemo enabled
  ;; ivy, swiper and counsel.
  :config
  (progn
    ;; `avy-migemo-mode' enables migemo search on find-file, grep, etc.!!
    (require 'avy-migemo-e.g.ivy)
    (require 'avy-migemo-e.g.swiper)
    (require 'avy-migemo-e.g.counsel)

    ;; HACK swiper--add-overlays は migemo で override されるが、swiper 側の
    ;; 以下修正により、migemo の関数が動かなくなっている。
    ;; (`swiper--add-overlay' という関数がいなくなっている)
    ;;
    ;; https://github.com/abo-abo/swiper/commit/773ac65ad73ea3207b4df93e098943ded5e330d2#diff-d0a13b1edafb7a77a69bf8e977f4d4e9
    ;;
    ;; これに対応するため仕方ないので migemo の advice 関数に対する before
    ;; advice で消えたはずの関数を再定義している。
    (defadvice! +avy-migemo--swiper--add-overlays-migemo-b (_args)
      :before #'swiper--add-overlays-migemo
      (defun swiper--add-overlay (beg end face wnd priority)
        (let ((overlay (make-overlay beg end)))
          (push overlay swiper--overlays)
          (overlay-put overlay 'face face)
          (overlay-put overlay 'window wnd)
          (overlay-put overlay 'priority priority))))

    (avy-migemo-mode t))
  :after (ivy counsel avy migemo))


;; 全角と半角の間に自動でスペースを入れてくれる
;; 参考: https://github.com/hlissner/doom-emacs/blob/10623868b8be7673fac765bb2ac6191ca44c344e/modules/input/japanese/config.el#L25
(use-package! pangu-spacing
  :hook (text-mode . pangu-spacing-mode)
  :init
  ;; replacing `chinese-two-byte' by `japanese'
  (setq pangu-spacing-chinese-before-english-regexp
        "\\(?1:\\cj\\)\\(?2:[0-9A-Za-z]\\)"
        pangu-spacing-chinese-after-english-regexp
        "\\(?1:[0-9A-Za-z]\\)\\(?2:\\cj\\)"
        ;; Always insert `real' space in text-mode including org-mode.
        pangu-spacing-real-insert-separtor t))


;; skk 、日本語入力
(use-package! ddskk
  :config
  (require 'skk-study)
  :hook (kill-emacs . skk-study-save)
  :bind
  (("<zenkaku-hankaku>" . skk-mode))
  :custom
  (skk-dcomp-activate t))
