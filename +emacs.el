;;; ~/.config/doom/+emacs.el -*- lexical-binding: t; -*-

;; マウスを無効化
;; https://stackoverflow.com/questions/4906534/disable-mouse-clicks-in-emacs#answer-4906698
(after! (dash s)
  (cl-flet  ((to-s (prefix n)
                   (--> (list prefix "mouse" n)
                        (-non-nil it)
                        (s-join "-" it)
                        `[,(make-symbol it)])))
    (let* ((prefixes '(nil "down" "drag" "double" "triple"))
           (numbers (-map 'number-to-string (number-sequence 1 5)))
           (keys (-table-flat #'to-s prefixes numbers)))
      (-each keys 'global-unset-key))))

;; modeline への時間表示
(progn
  (setq display-time-string-forms
        `((format "%s %s:%s  "
                  ,(all-the-icons-faicon "clock-o" :v-adjust 0.001)
                  24-hours minutes)))
  (display-time-mode +1))
